var gulp = require('gulp');
var ts = require('gulp-typescript');
var mocha = require('gulp-mocha');
var browserify = require('gulp-browserify');
var del = require('del');
var runSequence = require('run-sequence');

gulp.task('default', ['build']);

gulp.task('compile', function () {
    return gulp.src('src/**/*.ts')
        .pipe(ts({
            noImplicitAny: true,
            target: 'es6',
            // out: 'output.js',
            module: 'commonjs'
        }))
        .pipe(gulp.dest('src'));
});

gulp.task('build', function (cb) {
  runSequence(
    ['mocha'],
    ['cleants'],
    ['compile'],
    ['cleanDist'],
    ['browserify'],
    ['cleants'],
    // this callback is executed at the end, if any of the previous tasks errored, 
    // the first param contains the error
    function (err) {
      //if any error happened in the previous tasks, exit with a code > 0
      if (err) {
        var exitCode = 2;
        console.log('[ERROR] gulp build task failed');
        console.log('[FAIL] gulp build task failed - exiting with code ' + exitCode);
        return process.exit(exitCode);
      }
      else {
        return cb();
      }
    }
  );
});


gulp.task('browserify', function() {
    // Single entry point to browserify 
    return gulp.src('src/main.js')
        .pipe(browserify({
          insertGlobals : false
        }))
        .pipe(gulp.dest('./dist'))
});

//gulp.task('watchify', browserifyTask(bundleConfigs, true));
 
gulp.task('cleanDist', function(cb) {
    return del(['./dist/*'], {
        force: true
    }, cb);
});
 
gulp.task('cleants', function(cb) {
    return del(['./src/**/*.js'], {
        force: true
    }, cb);
});

gulp.task('mocha', function() {
    return gulp.src(['./tests/**/*.ts'])
    .pipe(mocha({
    	require: ['ts-node/register']
    }));
});
