# Typescript DEV #

This is evnironment for developement apps in Typescript with tests Mocha and build Gulp

##Installing the build  dependencies

* `sudo npm install -g gulp-cli`

* `npm install typescript gulp gulp-typescript del browserify gulp-typescript-browserify gulp-mocha gulp-browserify run-sequence -g`
* `npm install typescript gulp gulp-typescript del browserify gulp-typescript-browserify gulp-mocha gulp-browserify run-sequence --save-dev`

##Installing the development dependencies

* `npm install mocha chai ts-node -g`
* `npm install mocha chai ts-node --save-dev`

##Installing the type definitions

* `npm install typings -g`
* `npm install typings --save-dev`

* `typings install dt~mocha --global --save`
* `typings install npm~chai --save`

OR

* `npm install @types/chai @types/mocha --save-dev`

