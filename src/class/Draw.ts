class Draw
{
	private el     : HTMLCanvasElement;
	private context: CanvasRenderingContext2D;
	private width  : number;
	private height : number;

	private style  : boolean;

	constructor(id: string)
	{
		this.el      = <HTMLCanvasElement> document.getElementById(id);
		this.width   = this.el.width;
		this.height  = this.el.height;
		this.context = this.el.getContext("2d");
		this.clear();
		console.log('e', this.el);
		console.log('CTX', this.context);
	}

	public clear(): void
	{
		this.setInverse(false);
		this.context.fillRect(0, 0, this.width, this.height);
	}

	public setInverse(isInverse: boolean): void
	{
		this.style = isInverse;
		this.setStyle();
	}

	public setColor(color: string)
	{
		this.context.fillStyle = color;
	}

	private setStyle(): void
	{
		this.context.fillStyle = (this.style) ? "black" : "white";
	}

	public box(fromX: number, fromY: number, toX: number, toY: number): void
	{
		this.context.fillRect(fromX, fromY, toX, toY);
	}

	// private setPixel(imageData, x: number, y: number, r: number, g: number, b: number, a: number) 
	// {
	//     let index = (x + y * imageData.width) * 4;
	//     imageData.data[index+0] = r;
	//     imageData.data[index+1] = g;
	//     imageData.data[index+2] = b;
	//     imageData.data[index+3] = a;
	// }
}

export { Draw };