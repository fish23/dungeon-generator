import { IGeneratorMapCaveConfig } from '../interface/IGeneratorMapCaveConfig';
import { GeneratorMap } from './GeneratorMap';



class GeneratorMapCave extends GeneratorMap
{
	constructor(width: number, height: number)
	{
		super(width, height);
	}

	public generate(config: IGeneratorMapCaveConfig)
	{
		this.createCave(config);
	}
}
export { GeneratorMapCave }