import { IGeneratorMapCaveConfig } from '../interface/IGeneratorMapCaveConfig';
import { IGeneratorMapRectangleConfig } from '../interface/IGeneratorMapRectangleConfig';
import { IGeneratorMapCombinedConfig } from '../interface/IGeneratorMapCombinedConfig';

import { AStar } from './AStar';

class GeneratorMap
{
	protected width     : number;
	protected height    : number;

	protected aStar     : AStar;

	protected board     : Array<Array<number>>;
	protected boardEmpty: Array<Array<number>>;

	protected points    : Array<Array<number>>;

	protected items     : Array<number>;

	constructor(width: number, height: number)
	{
		this.width    = width;
		this.height   = height;
		this.createBoards();
	}

	public getBoard(): Array<Array<number>>
	{
		return this.board;
	}

	public export(): string
	{
		return JSON.stringify({board: this.board, points: this.points});
	}

	protected createBoards(): void
	{
		this.items      = [];
		this.board      = [];
		this.boardEmpty = [];
		for(let x = 0; x < this.width; x++)
		{
			let row      = [];
			let rowEmpty = [];
			for(let y = 0; y < this.height; y++)
			{
				rowEmpty.push(0);
				row.push(255);
			}
			this.boardEmpty.push(rowEmpty);
			this.board.push(row);
		}
		this.aStar = new AStar(this.boardEmpty, this.width, this.height, false);
	}

	protected createCave(config: IGeneratorMapCaveConfig): void
	{
		let xStartMax = (Math.random() > 0.4) ? true : false;
		let yStartMax = (Math.random() > 0.6) ? true : false;
		let last: Array<number> = [0, 0];
		last[0] = Math.floor(Math.random() * this.width);
		last[1] = Math.floor(Math.random() * this.height);
		let points: Array<Array<number>> = [last];
		for(let i = 0, len = config.items + 1; i < len; i++)
		{
			let point = this.getCavePointWithDraw(last, config.factorCave, config.spaceCave);
			points.push(point);
			last = point;
		}
		this.boardPlacePoints(points, config.enemy);
	}

	protected createCombined(config: IGeneratorMapCombinedConfig): void
	{
		let xStartMax = (Math.random() > 0.4) ? true : false;
		let yStartMax = (Math.random() > 0.6) ? true : false;
		let last: Array<number> = [0, 0];
		last[0] = Math.floor(Math.random() * this.width);
		last[1] = Math.floor(Math.random() * this.height);
		let points: Array<Array<number>> = [[last[0], last[1]]];
		for(let i = 0, len = config.items + 1; i < len; i++)
		{
			let isRectangle = (Math.random() > config.rectangleRatio);
			let isFirstLast = (i === 0 || i === (len - 1));
			let point = (isRectangle) ? this.getRectanglePointWithDraw(last, config.factorRectangle, config.roomSizeFactorRectangle, isFirstLast) : this.getCavePointWithDraw(last, config.factorCave, config.spaceCave);
			points.push([point[0], point[1]]);
			last = point;
		}
		this.boardPlacePoints(points, config.enemy);
	}

	protected createRectangle(config: IGeneratorMapRectangleConfig): void
	{
		let xStartMax = (Math.random() > 0.4) ? true : false;
		let yStartMax = (Math.random() > 0.6) ? true : false;

		let last: Array<number> = [0, 0];
		last[0] = Math.floor(Math.random() * this.width);
		last[1] = Math.floor(Math.random() * this.height);
		let points: Array<Array<number>> = [[last[0], last[1]]];
		for(let i = 0, len = config.items + 1; i < len; i++)
		{
			let isFirstLast = (i === 0 || i === (len - 1));
			let point = this.getRectanglePointWithDraw(last, config.factorRectangle, config.roomSizeFactorRectangle, isFirstLast);
			points.push([point[0], point[1]]);
			last = point;
		}
		this.boardPlacePoints(points, config.enemy);
	}

	protected getCavePointWithDraw(last: Array<number>, factor: number, space: number): Array<number>
	{
		let point = this.getRandomPointCave(last, factor);
		let isUsed = (false === this.boardPointSpace(point, space));
		let maxCount = 20;
		while(isUsed && --maxCount > 0)
		{
			point = this.getRandomPointCave(last, factor);
			isUsed = (false === this.boardPointSpace(point, space));
		}
		let path = this.aStar.find(last, point);
		for(let i = 0, len = path.length; i < len; i++)
		{
			this.board[path[i].x][path[i].y] = 0;
		}
		return point;
	}

	protected getRectanglePointWithDraw(last: Array<number>, factor: number, roomSizeFactor: number, isFirstLast: boolean): Array<number>
	{
		let space = 11;
		let point = this.getRandomPointRectangle(last, factor);
		let isUsed = (false === this.boardPointSpace(point, space));
		let maxCount = 999;
		while(isUsed && --maxCount > 0)
		{
			point = this.getRandomPointRectangle(last, factor);
			isUsed = (false === this.boardPointSpace(point, space));
		}
		let middlePointX = (Math.random() > 0.49);

		let middlePoint = (middlePointX) ? [last[0], point[1]] : [point[0], last[1]];
		if(middlePointX)
		{
			let yFrom = (last[1] > point[1]) ? point[1] : last[1];
			let yTo   = (last[1] > point[1]) ? last[1]    : point[1];
			for(let y = yFrom; y <= yTo; y++) this.board[last[0]][y] = 0;
			let xFrom = (last[0] > point[0]) ? point[0] : last[0];
			let xTo   = (last[0] > point[0]) ? last[0]    : point[0];
			for(let x = xFrom; x <= xTo; x++) this.board[x][point[1]] = 0;
		}
		else
		{
			let xFrom = (last[0] > point[0]) ? point[0] : last[0];
			let xTo   = (last[0] > point[0]) ? last[0]    : point[0];
			for(let x = xFrom; x <= xTo; x++) this.board[x][last[1]] = 0;

			let yFrom = (last[1] > point[1]) ? point[1] : last[1];
			let yTo   = (last[1] > point[1]) ? last[1]    : point[1];
			for(let y = yFrom; y <= yTo; y++) this.board[point[0]][y] = 0;
		}

		//if(false === isFirstLast && Math.random() > 0.3)
		if(false === isFirstLast)
		{
			let xMin  = 2;
			let yMin  = 2;
			let xSize = Math.max(Math.floor(Math.random() * (this.width * roomSizeFactor)), xMin);
			let ySize = Math.max(Math.floor(Math.random() * (this.height * roomSizeFactor)), yMin);
			if(xSize > 1 && ySize > 1)
			{
				let xFrom = Math.max(Math.floor(point[0] - (xSize / 2)), 1);
				let yFrom = Math.max(Math.floor(point[1] - (ySize / 2)), 1);
				let xTo = Math.min(Math.floor(point[0] + (xSize / 2)), this.width - 2);
				let yTo = Math.min(Math.floor(point[1] + (ySize / 2)), this.height - 2);
				for(let x = xFrom; x <= xTo; x++) for(let y = yFrom; y <= yTo; y++) this.board[x][y] = 0;
			}
		}
		return point;
	}

	protected boardPointSpace(point: Array<number>, space: number): boolean
	{
		if(space <= 1) return true;
		let xFrom = Math.max(Math.round(point[0] - (space / 2)), 1);
		let yFrom = Math.max(Math.round(point[1] - (space / 2)), 1);
		let xTo = Math.min(Math.round(point[0] + (space / 2)), this.width - 2);
		let yTo = Math.min(Math.round(point[1] + (space / 2)), this.height - 2);
		for(let x = xFrom; x <= xTo; x++) for(let y = yFrom; y <= yTo; y++) if(this.board[x][y] !== 0) return false;
		return true;
	}

	protected boardPlacePoints(points: Array<Array<number>>, enemy: number): void
	{
		this.points = points;
		let len = points.length;
		let enemiesLeft = Math.max((len - 2) * enemy, 0);
		let enemyId     = 101;
		let enemiesPlaces: Array<number> = [];
		let enemyVariance = 2;
		for(let i = 0, len = points.length; i < len; i++)
		{
			let point = points[i];
			let value = i;
			if(i === 0 || i === (len - 1))
			{
				value = (i === 0) ? 100 : 200;
			}
			else if(enemiesLeft > 0 && Math.random() >= enemy)
			{
				let isPlaced = false;
				while(!isPlaced)
				{
					let varianceX = Math.floor(Math.random() * enemyVariance) + 1;
					let varianceY = Math.floor(Math.random() * enemyVariance) + 1;
					let x = (Math.random() > 0.5) ? Math.max(point[0]-varianceX, 0) : Math.min(point[0]+varianceX, this.width);
					let y = (Math.random() > 0.5) ? Math.max(point[1]-varianceY, 0) : Math.min(point[1]+varianceY, this.width);
					let current = this.board[x][y];
					if(current === 0)
					{
						this.board[x][y] = enemyId++;
						enemiesLeft--;
						enemiesPlaces.push(i);
						isPlaced = true;
					}
				}
			}
			this.board[point[0]][point[1]] = value;
		}
	}

	protected getRandomPointCave(last: Array<number>, factor: number): Array<number>
	{
		let xSub = (last[0] < (this.width * 0.05) || Math.random() > 0.5) ? true : false;
		let ySub = (last[1] < (this.height * 0.05) || Math.random() > 0.5) ? true : false;

		let x       = last[0];
		let y       = last[1];
		let xOffset = Math.floor((Math.random() * (this.width * factor)) + this.width / 10);
		let yOffset = Math.floor((Math.random() * (this.height * factor)) + this.height / 10);
		if(xSub)
		{
			x = Math.min(x + xOffset, this.width - 2);
		}
		else
		{
			x = Math.max(x - xOffset, 1);
		}
		if(ySub)
		{
			y = Math.min(y + yOffset, this.height - 2);
		}
		else
		{
			y = Math.max(y - yOffset, 1);
		}
		return [x,y];
	}

	protected getRandomPointRectangle(last: Array<number>, factor: number): Array<number>
	{
		let xRandom = Math.floor(Math.random() * (this.width - 2)) + 1;
		let yRandom = Math.floor(Math.random() * (this.height - 2)) + 1;
		return [xRandom, yRandom];
	}

}

export { GeneratorMap };