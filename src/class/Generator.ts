import { AStar } from './AStar';
import { Draw } from './Draw';

import { GeneratorMapCave } from './GeneratorMapCave';
import { GeneratorMapRectangle } from './GeneratorMapRectangle';
import { GeneratorMapCombined } from './GeneratorMapCombined';

import { IGeneratorMapCaveConfig } from '../interface/IGeneratorMapCaveConfig';
import { IGeneratorMapRectangleConfig } from '../interface/IGeneratorMapRectangleConfig';
import { IGeneratorMapCombinedConfig } from '../interface/IGeneratorMapCombinedConfig';

interface ItemAction
{
	id: number;
	name: string;
}

interface ItemType
{
	id  : number;
	name: string;
	action: ItemAction
};

class Generator
{
	private id        : string;
	private idCanvas  : string;
	private idItem    : string;
	private el        : HTMLElement;	
	private width     : number;
	private height    : number;

	private pixelSize : number = 20;

	private board     : Array<Array<number>>;
	private draw      : Draw;
	private itemsActions: { [id: string]: ItemAction };

	private items     : { [id: string]: ItemType };
	private mapColors : Array<string>;
	private mapToogleMode: boolean = false;
	private mapToogle: any;
	private mapToogleIsWall: boolean;

	constructor(id: string, width: number, height: number)
	{

		let el      = document.getElementById(id);
		if(!el)
		{
			throw Error('Generator: Invalid ID!');
		}
		this.el       = el;
		this.id       = id;
		this.idCanvas = id + '-canvas';
		this.idItem   = 'item';
		this.width    = width;
		this.height   = height;
		this.generateSets();
		this.createHTML();
		this.draw   = new Draw(this.idCanvas);

		let mapGenerator = new GeneratorMapCombined(this.width, this.height);
		let mapCombinedConfig: IGeneratorMapCombinedConfig = {
			items: 10,
			enemy: 0.5,
			factorCave: 0.2,
			spaceCave: 10,

			rectangleRatio: 0.5,

			factorRectangle: 0.3,
			spaceRectangle: 12,
			roomSizeFactorRectangle: 0.11,
		}
		mapGenerator.generate(mapCombinedConfig);
		this.board = mapGenerator.getBoard();
		console.log('map', mapGenerator.export());
		this.refresh();
	}

	private generateSets()
	{
		this.itemsActions = {
	        '0': {
	        	id: 0,
	        	name: 'wall'
	        },
	        '1': {
	        	id: 1,
	        	name: 'floor'
	        },
	        '2': {
	        	id: 2,
	        	name: 'entrance'
	        },
	        '3': {
	        	id: 3,
	        	name: 'entrance_next'
	        },
	        '4': {
	        	id: 4,
	        	name: 'item'
	        },
	        '5': {
	        	id: 5,
	        	name: 'enemy'
	        },
	        '6': {
	        	id: 6,
	        	name: 'interact'
	        }
    	};

		this.items = {
			'0': {
				id: 0,
				name: 'Floor',
				action: this.itemsActions[1]
			},
			'100': {
				id: 100,
				name: 'Start',
				action: this.itemsActions[2]
			},
			'200': {
				id: 200,
				name: 'Next level',
				action: this.itemsActions[3]
			},
			'255': {
				id: 255,
				name: 'Wall',
				action: this.itemsActions[0]
			}
	    };
	    for(let itemId = 1, len = 10 /*99*/; itemId <= len; itemId++)
	    {
	    	let key = itemId.toString();
			this.items[key] = {
				id: itemId,
				name: 'Item ' + itemId.toString(),
				action: this.itemsActions[4]
			};
	    }

	    for(let enemyId = 101, len = 110 /*199*/; enemyId <= len; enemyId++)
	    {
	    	let key = enemyId.toString();
			this.items[key] = {
				id: enemyId,
				name: 'Enemy ' + enemyId.toString(),
				action: this.itemsActions[5]
			};
	    }

	    for(let interactId = 201, len = 205 /*250*/; interactId <= len; interactId++)
	    {
	    	let key = interactId.toString();
			this.items[key] = {
				id: interactId,
				name: 'Interact ' + interactId.toString(),
				action: this.itemsActions[6]
			};
	    }

		let colors: Array<string> = [];
		for(let c = 0; c < 255; c++)
		{
			if(c === 100 || c === 200)
			{
				let color = (c === 100) ? '#ff0000' : '#00ff00';
				colors.push(color);
				continue;
			}
			let parts: Array<string> = [];
			for(let i = 0; i < 3; i++)
			{
				let part = (Math.floor(Math.random() * 160) + 64);
				parts.push(part.toString(16));
			}
			let color = '#' + parts.join('');
			colors.push(color);
		}
		this.mapColors = colors;
	}

	private createHTML(): void
	{
		let widthPixels    = this.width * this.pixelSize;
		let heightPixels   = this.height * this.pixelSize;
		let widthPixelsItem = '450px';
		let heightPixelsItem = '700px';
		let canvasHTML     = `<canvas id="${this.idCanvas}" width="${widthPixels}" height="${heightPixels}"></canvas>`;
		let itemHTML       = `<div id="${this.idItem}" width="${widthPixelsItem}" height="${heightPixelsItem}">`;
		itemHTML          += `<h2 id="${this.idItem}-name"></h2>`;
		itemHTML          += `<p>Coords: <span id="${this.idItem}-x"></span>, <span id="${this.idItem}-y"></span><p>`;
		itemHTML          += `<select id="${this.idItem}-type">`;
		for(let id in this.items)
		{
			console.log('id',id);
			let item = this.items[id];
			console.log('item',item);
			itemHTML          += `<option value="${id}">${item['name']}</option>`;
		}
		itemHTML          += `</select>`;
		itemHTML          += `</div>`;
		itemHTML          += `</div>`;
		let controlsHTML   = `<div id="controls">`;
		
		controlsHTML      += `<label>factorRectangle</label><input id="factorRectangle" type="text" value="0.3" />`;
		controlsHTML      += `<label>spaceRectangle</label><input id="spaceRectangle" type="text" value="12" />`;
		controlsHTML      += `<label>roomSizeFactorRectangle</label><input id="roomSizeFactorRectangle" type="text" value="0.11" /><br />`;
		controlsHTML      += `<label>items</label><input id="items" type="text" value="13" />`;
		controlsHTML      += `<label>enemy</label><input id="enemy" type="text" value="0.5" />`;
		controlsHTML      += `<label>rectangleRatio</label><input id="rectangleRatio" type="text" value="0.5" />`;
		controlsHTML      += `<div id="MapCombined" style="display:block-inline;border:1px solid #333;padding: 3px; width: 100px;">Refresh</div>`
		controlsHTML      += `</div>`;
		this.el.innerHTML  = canvasHTML;
		this.el.innerHTML += itemHTML;
		this.el.innerHTML += controlsHTML;
		document.getElementById('MapCombined').addEventListener('click', this.processRefreshEvent.bind(this));
		document.getElementById(this.idCanvas).addEventListener('mousedown', this.processEditMap.bind(this));
		document.getElementById(this.idCanvas).addEventListener('mousemove', this.processToogleMap.bind(this));
		document.getElementById(this.idCanvas).addEventListener('mouseup', this.processDisableToogleMap.bind(this));
		document.getElementById(this.idItem+'-type').addEventListener('change', this.processChangeItem.bind(this));
	}

	private processEditMap(event: MouseEvent)
	{
		//console.log(event);
		event.preventDefault();
		let canvas = document.getElementById(this.idCanvas);
		let x = Math.floor((event.layerX / canvas.clientWidth) * this.width);
		let y = Math.floor((event.layerY / canvas.clientHeight) * this.height);
		let item = this.board[x][y].toString();
		if(event.shiftKey === true && (item === '0' || item === '255'))
		{
			this.toogleWallFloor(x, y, (item === '255'));
			this.mapToogleIsWall = (item === '255');
		}
		this.selectItem(x, y);
		/* on change select 
		let select: any = document.getElementById(this.idItem+'-type');
		select.value = item;
		// select.options[select.selectedIndex].value = item;
		let selectedId = select.options[select.selectedIndex].value;
		console.log(x, y);
		console.log(item);
		console.log('id sel', selectedId);
		//console.log(canvas.clientWidth);
		//this.board[x][y]
		*/
	}

	private processChangeItem(event: Event)
	{
		event.preventDefault();
		let select: any = document.getElementById(this.idItem+'-type');
		let x = parseInt(document.getElementById(this.idItem+'-x').innerHTML) - 1;
		let y = parseInt(document.getElementById(this.idItem+'-y').innerHTML) - 1;
		let item = this.board[x][y].toString();
		// select.value = item;
		// select.options[select.selectedIndex].value = item;
		let selectedId = select.options[select.selectedIndex].value;
		this.board[x][y] = parseInt(selectedId);
		console.log('id sel', selectedId);
		console.log('x: '+x.toString(), 'y: '+y.toString());
		this.refresh();
		this.selectItem(x, y);
	}

	private processToogleMap(event: MouseEvent)
	{
		event.preventDefault();
		if(!this.mapToogleMode) return;
		let canvas = document.getElementById(this.idCanvas);
		let x = Math.floor((event.layerX / canvas.clientWidth) * this.width);
		let y = Math.floor((event.layerY / canvas.clientHeight) * this.height);
		if(x === this.mapToogle[0] && y === this.mapToogle[1]) return;
		let item = this.board[x][y].toString();
		if(event.shiftKey === true && ((item === '0' && !this.mapToogleIsWall) || (item === '255' && this.mapToogleIsWall)))
		{
			this.toogleWallFloor(x, y, (item === '255'));
		}
		this.selectItem(x, y);
	}

	private processDisableToogleMap(event: MouseEvent)
	{
		event.preventDefault();
		if(!this.mapToogleMode) return;
		this.mapToogleMode = false;
		let canvas = document.getElementById(this.idCanvas);
		let x = Math.floor((event.layerX / canvas.clientWidth) * this.width);
		let y = Math.floor((event.layerY / canvas.clientHeight) * this.height);
		this.selectItem(x, y);
	}

	private selectItem(x: number, y: number)
	{
		let item = this.board[x][y].toString();
		document.getElementById(this.idItem + '-x').innerHTML = (x + 1).toString();
		document.getElementById(this.idItem + '-y').innerHTML = (y + 1).toString();
		document.getElementById(this.idItem + '-name').innerHTML = `${this.items[item]['name']} [ID: ${item}]`;

		//itemDetail.action.name
		let itemDetail = this.items[item];
		console.log('itemDetail', itemDetail.action.name);
		document.getElementById(this.idItem).setAttribute('class', 'icon ' + itemDetail.action.name);
		let select: any = document.getElementById(this.idItem+'-type');
		select.value = item;
		console.log(x, y);
		console.log(item);
	}

	private toogleWallFloor(x: number, y: number, isWall: boolean)
	{
		this.board[x][y] = (isWall) ? 0 : 255;
		this.mapToogleMode = true;
		this.mapToogle = [x, y];
		this.refresh();
	}

	private processRefreshEvent(event: Event)
	{
		let factorRectangle = <HTMLInputElement> document.getElementById('factorRectangle');
		let spaceRectangle = <HTMLInputElement> document.getElementById('spaceRectangle');
		let roomSizeFactorRectangle = <HTMLInputElement> document.getElementById('roomSizeFactorRectangle');
		let rectangleRatio = <HTMLInputElement> document.getElementById('rectangleRatio');
		let items = <HTMLInputElement> document.getElementById('items');
		let enemy = <HTMLInputElement> document.getElementById('enemy');

		let factorRectangleValue = parseFloat(factorRectangle.value);
		let spaceRectangleValue = parseInt(spaceRectangle.value);
		let roomSizeFactorRectangleValue = parseFloat(roomSizeFactorRectangle.value);
		let rectangleRatioValue = parseFloat(rectangleRatio.value);
		let itemsValue = parseInt(items.value);
		let enemyValue = parseFloat(enemy.value);

		let mapGenerator = new GeneratorMapCombined(this.width, this.height);
		let mapCombinedConfig: IGeneratorMapCombinedConfig = {
			items: itemsValue,
			enemy: enemyValue,
			factorCave: 0.2,
			spaceCave: 10,

			rectangleRatio: rectangleRatioValue,

			factorRectangle: factorRectangleValue,
			spaceRectangle: spaceRectangleValue,
			roomSizeFactorRectangle: roomSizeFactorRectangleValue,
		}
		console.log('mapCombinedConfig', mapCombinedConfig);
		console.log('this.width', this.width);
		console.log('this.height', this.height);
		mapGenerator.generate(mapCombinedConfig);
		this.board = mapGenerator.getBoard();
		console.log('map', mapGenerator.export());
		this.refresh();
	}

	public refresh(): void
	{
		console.log('refresh', this);
		for(let x = 0; x < this.width; x++)
		{
			for(let y = 0; y < this.height; y++)
			{
				let color = this.mapColors[this.board[x][y]];
				this.drawPixel(x, y, color);
			}
		}
		this.drawGrid();
	}

	private drawPixel(x: number, y: number, color: string): void
	{
		let pixel = (this.board[x][y] === 255) ? true : false;
		this.draw.setInverse(pixel);
		let xPixel = x * this.pixelSize;
		let yPixel = y * this.pixelSize;
		if(this.board[x][y] > 0 && !pixel)
		{
			this.draw.setColor(color);
		}
		this.draw.box(xPixel, yPixel, xPixel + this.pixelSize, yPixel + this.pixelSize);
	}

	private drawGrid(): void
	{
		this.draw.setColor("#779955");
		for(let y = 0; y < this.height; y++)
		{
			let xPixel = (this.width) * this.pixelSize;
			let yPixel = y * this.pixelSize;
			this.draw.box(0, yPixel, xPixel, 1);
		}

		for(let x = 0; x < this.width; x++)
		{
			let xPixel = x * this.pixelSize;
			let yPixel = (this.height) * this.pixelSize;;
			this.draw.box(xPixel, 0, 1, yPixel);
		}
	}
}

export { Generator };