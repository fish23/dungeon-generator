import { IAStarNode } from '../interface/IAStarNode';

class AStar 
{
	/*
	Copyright (C) 2009 by Benjamin Hardin

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
	*/
	private result: Array<IAStarNode>;

	private board: Array<Array<number>>;

	private columns: number;

	private rows: number;

	private allowDiagonals: boolean;

	constructor(board: Array<Array<number>>, columns: number, rows: number, allowDiagonals: boolean)
	{
		this.board          = board;
		this.columns        = columns;
		this.rows           = rows;
		this.allowDiagonals = allowDiagonals;
	}

	public setDiagonals(value: boolean): void
	{
		this.allowDiagonals = value;
	}

	public setBoard(board: Array<Array<number>>, columns: number, rows: number): void
	{
		this.board          = board;
		this.columns        = columns;
		this.rows           = rows;
	}

	public find(start: Array<number>, destination: Array<number>): Array<IAStarNode>
	{
		if( this.board[ destination[0] ][ destination[1] ] == 1 )
		{
			this.result = [];
			return this.result;
		}

		//Create start and destination as true nodes
		let startNode: IAStarNode       = this.getStarNode(start[0], start[1], -1, -1, -1, -1);
		let destinationNode: IAStarNode = this.getStarNode(destination[0], destination[1], -1, -1, -1, -1);

		let open: Array<IAStarNode> = []; //List of open nodes (nodes to be inspected)
		let closed: Array<IAStarNode> = []; //List of closed nodes (nodes we've already inspected)

		let g = 0; //Cost from start to current node
		let h = this.getStarHeuristic(startNode, destinationNode); //Cost from current node to destinationNode
		let f = g + h; //Cost from startNode to destinationNode going through the current node

		//Push the startNode node onto the list of open nodes
		open.push(startNode); 

		//Keep going while there's nodes in our open list
		while (open.length > 0)
		{
			//Find the best open node (lowest f value)

			//Alternately, you could simply keep the open list sorted by f value lowest to highest,
			//in which case you always use the first node
			let bestCost = open[0].f;
			let bestNode = 0;

			for(let i = 1, len = open.length; i < len; i++)
			{
				if (open[i].f < bestCost)
				{
					bestCost = open[i].f;
					bestNode = i;
				}
			}

			//Set it as our current node
			let node = open[bestNode];

			//Check if we've reached our destinationNode
			if(node.x == destinationNode.x && node.y == destinationNode.y)
			{
				let path = [destinationNode]; //Initialize the path with the destinationNode node

				//Go up the chain to recreate the path 
				while (node.parentIndex != -1)
				{
					node = closed[node.parentIndex];
					path.unshift(node);
				}
				this.result = path;
				return this.result;
			}

			//Remove the current node from our open list
			open.splice(bestNode, 1);

			//Push it onto the closed list
			closed.push(node);

			//Expand our current node (look in all 8 directions)
			for(let newNodeX = Math.max(0, node.x - 1); newNodeX <= Math.min(this.columns - 1, node.x + 1); newNodeX++)
			{
				for(let newNodeY = Math.max(0, node.y - 1); newNodeY <= Math.min(this.rows - 1, node.y + 1); newNodeY++)
				{
					if (!this.allowDiagonals)
					{
						if (newNodeX != node.x && newNodeY != node.y)
							continue;
					}
					if (this.board[newNodeX][newNodeY] == 0 //If the new node is open
						|| (destinationNode.x == newNodeX && destinationNode.y == newNodeY)) //or the new node is our destinationNode
					{
						//See if the node is already in our closed list. If so, skip it.
						let foundInClosed = false;
						for (let i in closed)
						{
							if (closed[i].x == newNodeX && closed[i].y == newNodeY)
							{
								foundInClosed = true;
								break;
							}
						}
						if (foundInClosed)
						{
							continue;
						}
						//See if the node is in our open list. If not, use it.
						let foundInOpen = false;
						for (let i in open)
						{
							if (open[i].x == newNodeX && open[i].y == newNodeY)
							{
								foundInOpen = true;
								break;
							}
						}
						if (!foundInOpen)
						{
							let newNode = this.getStarNode(newNodeX, newNodeY, closed.length - 1, -1, -1, -1);
							newNode.g   = node.g + Math.floor( Math.sqrt( Math.pow(newNode.x-node.x, 2) + Math.pow(newNode.y-node.y, 2) ) );
							newNode.h   = this.getStarHeuristic(newNode, destinationNode);
							newNode.f   = newNode.g + newNode.h;
							open.push(newNode);
						}
					}
				}
			}
		}
		this.result = [];
		return this.result;
	}

	private getStarNode(x: number, y: number, parentIndex: number, g: number, h: number, f: number): IAStarNode
	{
		let node: IAStarNode = {
			x          : x,
			y          : y,
			g          : g,
			h          : h,
			f          : f,
			parentIndex: parentIndex
		};
		return node;
	}

	//An A* heurisitic must be admissible, meaning it must never overestimate the distance to the goal.
	//In other words, it must either underestimate or return exactly the distance to the goal.
	private getStarHeuristic(node: IAStarNode, destination: IAStarNode): number
	{
		//Find the straight-line distance between the current node and the destination. (Thanks to id for the improvement)
		//return Math.floor(Math.sqrt(Math.pow(node.x-destination.x, 2)+Math.pow(node.y-destination.y, 2)));
		let x = node.x - destination.x;
		let y = node.y - destination.y;
		return x * x + y * y;
	}
}

export { AStar };