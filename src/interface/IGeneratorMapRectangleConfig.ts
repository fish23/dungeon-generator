interface IGeneratorMapRectangleConfig
{
	items                  : number;
	enemy                 ?: number;
	factorRectangle        : number;
	spaceRectangle         : number;
	roomSizeFactorRectangle: number;
}

export { IGeneratorMapRectangleConfig };