interface IGeneratorMapCaveConfig
{
	enemy?: number;
	items: number;
	factorCave: number;
	spaceCave: number;
}

export { IGeneratorMapCaveConfig };