interface IGeneratorMapCombinedConfig
{
	items: number;
	enemy: number;
	factorCave: number;
	spaceCave: number;

	rectangleRatio: number;

	factorRectangle: number;
	spaceRectangle: number;
	roomSizeFactorRectangle: number;
}

export { IGeneratorMapCombinedConfig };