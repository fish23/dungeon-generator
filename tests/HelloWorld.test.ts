import { HelloWorld } from '../src/class/HelloWorld';
import { expect } from 'chai';
// if you used the '@types/mocha' method to install mocha type definitions, uncomment the following line
import 'mocha';

describe('HelloWorld class', () => {
  it('should return hello world', () => {
  	let helloWorld = new HelloWorld();
    const result = helloWorld.get();
    expect(result).to.equal('Hello World!');
  });
});
